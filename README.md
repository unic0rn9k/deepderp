# Deep Derp - deep learning framework ![logo](./logo.png)

### not maintained 

deepderp is a minimalist deep learning framework, that i mostly just worked on for the experience.
note that deepderp does use the rand library and does not seed it, so if you want it seeded you need to do so yourself.

### install:

`go get gitlab.com/unic0rn9k/album`

`go get gitlab.com/unic0rn9k/deepderp`

### example code:

```go
package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"gitlab.com/unic0rn9k/deepderp"
)

func main() {
	var conf deepderp.Config

	conf.NeuronsInLayers = make([]int, 3) // set the neural network to have 3 layers
	conf.NeuronsInLayers[0] = 10 // set the neural network to have 10 neurons in the first layer
	conf.NeuronsInLayers[1] = 10 // set the neural network to have 10 neurons in the second layer
	conf.NeuronsInLayers[3] = 2 // set the neural network to have 3 neurons in the final layer
	conf.Lr = 0.01 // set the learning rate of the neural network to be equal to 0.001
	conf.Inputs = 10 // set the neural network to have 10 inputs
    // NOTE:  the neural network will have as many outputs as there are neurons in the last layer.

	dnn := deepderp.DNN{} // create the neural network
	deepderp.InitDNN(&dnn, conf) // initialize the neural network

	var inputs []float64 // load test inputdata from json file "inputs.json"
	inputDataRaw, _ := ioutil.ReadFile("./inputs.json") 
	json.Unmarshal(inputDataRaw, &inputs)

	out, _ := deepderp.FeedForward(dnn, inputs) //feed the inputs to the neural network

	fmt.Println("untrained output:", out)

	var trainingDataSets []struct { // load test training data from json file
		inputs      []float64
		expectedOut []float64
	}
	trainingDataRaw, _ := ioutil.ReadFile("./training_data.json")
	json.Unmarshal(trainingDataRaw, &trainingDataSets)

	for _, trainingData := range trainingDataSets { // train the neural network on the training data
		deepderp.BackProbagateNet(&dnn, trainingData.expectedOut, trainingData.inputs) // back probagate the neural network
	}

	fmt.Println("trained output:", out)
}

```

