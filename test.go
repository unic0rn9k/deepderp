package deepderp

import (
	"fmt"

	"gitlab.com/unic0rn9k/album"
)

//NOTE: No code in this lib should be used in any actual code!

//Test is a function for testing libs.
func Test() { /*
		inputs := album.AlbumFillWith(1, 10)
		expectedOut := 1.0
		testConf := Config{}
		testConf.Functions = Functions{math.Tanh, sech2, CostF, derivedCost, true}
		testConf.Inputs = 10
		testConf.Lr = 0.1
		testConf.NeuronsInLayers = []int{2, 2, 2}
		testNeuron := Neuron{}
		testNeuron.init(testConf.Inputs)
		a := testNeuron.OutInputSlopeUpdated(inputs, expectedOut, testConf)
		b := testNeuron.OutInputSlope(inputs, expectedOut, testConf)
		fmt.Println("updated function:", a)
		fmt.Println("old     function:", b)

		if album.AsAlbum(a).IsEqualTo(b) {
			fmt.Println("a == b")
		} else {
			fmt.Println("a != b")
		} */
	
	album.RandomNumAlbum(1000, 100).Print()
//	album.RandomNumAlbum(1000,100).SaveAsImage("test.jpg")
//	album.RandomNumAlbum(1000,100).SaveAsImage("test2.jpg")
	
	fmt.Println("-----------------------------------------")
	labels := album.RandomNumAlbum(10, 1)
	RNNTestTypes := make([]MultiLayer, 3)
	for n := range RNNTestTypes {
		RNNTestTypes[n] = RNNLayer{}
	}
	RNNTest := MakeMultiNetwork(1, []int{16, 16, 1}, album.AlbumFillWith(0.1, 3), nil, RNNTestTypes)
	for n := range RNNTest {
		fmt.Println(":", len(RNNTest[n].Layer[0].Weights), ":", len(RNNTest[n].Layer))
	}

	for x := range labels {
		_, res := MultiFeedForward(&RNNTest, []float64{float64(len(labels))})
		fmt.Println(x, ":", res, "/", labels[x])
		for y := range RNNTest {
			vectorizedData := RNNTest[y].NN.VectorizedData([]album.NumAlbum{})
			NNTMP := RNNLayer{vectorizedData[0], vectorizedData[1]}
			NNTMP.Memorize()
			RNNTest[y].NN = NNTMP
		}
	}

	for trainingCycle := 0; trainingCycle < 1000; trainingCycle++ {
		for x := range labels {
			MultiBackProp(&RNNTest, []float64{float64(len(labels))}, []float64{labels[x]})
			for y := range RNNTest {
				vectorizedData := RNNTest[y].NN.VectorizedData([]album.NumAlbum{})
				NNTMP := RNNLayer{vectorizedData[0], vectorizedData[1]}
				NNTMP.Memorize()
				RNNTest[y].NN = NNTMP
			}
		}
	}

	for x := range labels {
		_, res := MultiFeedForward(&RNNTest, []float64{float64(len(labels))})
		fmt.Println(x, ":", res, "/", labels[x])
		for y := range RNNTest {
			vectorizedData := RNNTest[y].NN.VectorizedData([]album.NumAlbum{})
			NNTMP := RNNLayer{vectorizedData[0], vectorizedData[1]}
			NNTMP.Memorize()
			RNNTest[y].NN = NNTMP
		}
	} 
	
	fmt.Println("RNN test ------------------------------------")
	testRNN := NewRNNLayer(LayerConf{Functions{},0.1,1,1})
	fmt.Println(testRNN.NN.Predict(testRNN, []float64{1.0}))
	
	fmt.Println("old/normal dnn test -------------------------")
	testDNN := DNN{}
	InitDNN(&testDNN, Config{[]int{2, 2, 1}, 1, 0.1, Functions{}})
	out, _ := FeedForward(testDNN, []float64{1})
	fmt.Println(out, "/", 1)
	for n := 0; n < 100; n++ {
		BackProbagateNet(&testDNN, []float64{1}, []float64{1})
	}
	out, _ = FeedForward(testDNN, []float64{1})
	fmt.Println(out, "/", 1)
}
