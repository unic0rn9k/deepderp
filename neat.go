package deepderp

/*
  #
 #:#                  ##
 #: #    ###   ####  #: #
 #:  #  ##: #  #: #  #: #
 #:   # #:  #  #: #  #: #
 #:   # ####  #####  ###
 #:   # #:    #: #   #:
 #:  ## #:    #:  #  #:
 #:  #   ###  #:   # #:
 ####         #:     #:
                     #:

*/

//NEAT: who needs backpropagation and consistently labled data?

//NEAT is a struct containing a neat model that can be used to apply an evolutionary process to a neural network of a strict or mutable type
type NEAT struct {
	Population [][]Multi
	Apex       []Multi
	Conf       struct {
		PopulationSize int
		Genders        int
		mutationRate   int
	}
}
