package deepderp

import (
	"fmt"
	"math"

	"gitlab.com/unic0rn9k/album"
)

/*
  #
 #:#                  ##
 #: #    ###   ####  #: #
 #:  #  ##: #  #: #  #: #
 #:   # #:  #  #: #  #: #
 #:   # ####  #####  ###
 #:   # #:    #: #   #:
 #:  ## #:    #:  #  #:
 #:  #   ###  #:   # #:
 ####         #:     #:
                     #:

*/

//LayerConf is the same as Config but for individual layers
type LayerConf struct {
	Functs  Functions
	Lr      float64
	Inputs  int
	Outputs int
}

//LayerConf2Conf converts layerconf to conf
func LayerConf2Conf(in LayerConf) Config {
	out := Config{}
	out.Functions = in.Functs
	out.Lr = in.Lr
	return out
}

//MultiLayer is a self contained nn layer type that can be put in an array with other layers of diffrent types and used as a singular neural network with some universal functions
type MultiLayer interface {
	Init(*Multi)
	//                        in       out
	Predict(Multi, album.NumAlbum) album.NumAlbum
	//                        in       exout            slope
	OutProp(*Multi, album.NumAlbum, album.NumAlbum) album.NumAlbum
	//                        in       slope            slope
	HiddenProp(*Multi, album.NumAlbum, album.NumAlbum) album.NumAlbum
	//returns/sets any data that might be needed for special functions involving the underlying struct in the MultiLayer
	VectorizedData([]album.NumAlbum) []album.NumAlbum
}

//Multi single multi layer with a deffinable type
type Multi struct {
	Layer NeuralLayer
	Conf  LayerConf
	NN    MultiLayer
}

//Init initiates the multi and should be run before using it
func (multi *Multi) Init(conf LayerConf, layerType MultiLayer) {
	if !conf.Functs.isInUse {
		conf.Functs = Functions{math.Tanh, sech2, CostF, derivedCost, true}
	}
	if multi == nil {
		*multi = Multi{}
	}
	multi.Conf = conf
	multi.NN = layerType
	multi.NN.Init(multi)
	multi.Layer = make([]Neuron, multi.Conf.Outputs)
	for n := range multi.Layer {
		multi.Layer[n].init(multi.Conf.Inputs)
	}
}

//MultiFeedForward predicts an output for a MultiLayer
func MultiFeedForward(dnn *[]Multi, input album.NumAlbum) ([]album.NumAlbum, album.NumAlbum) {
	history := make([]album.NumAlbum, len(*dnn))
	album := input
	for n := range *dnn {
		tmp := (*dnn)[n].NN.Predict((*dnn)[n], album)
		album = tmp
		history[n] = album
	}
	return history, album
}

//MultiBackProp is a BackPropagation function for MultiLayer
func MultiBackProp(dnn *[]Multi, input, expectedOut album.NumAlbum) {
	outputAlbums, _ := MultiFeedForward(dnn, input)

	nextLayerSlope := (*dnn)[len(*dnn)-1].NN.OutProp(&((*dnn)[len(*dnn)-1]), outputAlbums[len(*dnn)-2], expectedOut)
	for n := len(*dnn) - 2; n >= 0; n-- {
		nextLayerSlope = (*dnn)[n].NN.HiddenProp(&((*dnn)[n]), outputAlbums[len(*dnn)-2], nextLayerSlope)
	}
}

//MultiOutBackProbagate backpropagation function for output neurons
func (dnn *Multi) MultiOutBackProbagate(inputs []float64, expectedOut float64, conf LayerConf) []float64 {
	out := (*dnn).NN.Predict(*dnn, inputs)
	slopes := album.NewNumAlbum(conf.Inputs)

	for n := range (*dnn).Layer {
		var neo *Neuron
		neo = &((*dnn).Layer[n])

		gradients := func(inputs []float64, expectedOut float64, conf LayerConf) []float64 {
			gradientVector := make([]float64, len(neo.Weights))
			weightedSum := album.DotAlbum(neo.Weights, inputs) + neo.Bias

			for n := range neo.Weights {
				gradientVector[n] = inputs[n] * conf.Functs.derivedActivation(weightedSum) * conf.Functs.derivedCost(expectedOut, out[n])
			}

			return gradientVector
		}(inputs, expectedOut, conf)

		neo.Lock()
		for n := range neo.Weights {
			neo.Weights[n] += -1 * gradients[n] * conf.Lr
		}

		neo.Bias += -1 * neo.OutBiasGradient(inputs, expectedOut, LayerConf2Conf(conf)) * conf.Lr
		neo.Unlock()

		slopes.AddAlbum(neo.OutInputSlope(inputs, expectedOut, LayerConf2Conf(conf)))
	}
	return slopes
}

//OutInputSlopeUpdated returns input slope, which is the sensitivity of the input in respect to the output
func (neo Neuron) OutInputSlopeUpdated(inputs album.NumAlbum, expectedOut float64, conf Config) album.NumAlbum {
	out := neo.F(inputs, conf)
	weightedSum := album.DotAlbum(neo.Weights, inputs) + neo.Bias

	gradientVector := neo.Weights.Scale(conf.Functions.derivedActivation(weightedSum)).Scale(conf.Functions.derivedCost(expectedOut, out))

	return gradientVector
}

//OutGradientUpdated returns gradient vector
func (neo Neuron) OutGradientUpdated(inputs album.NumAlbum, expectedOut float64, conf Config) album.NumAlbum {
	out := neo.F(inputs, conf)
	weightedSum := album.DotAlbum(neo.Weights, inputs) + neo.Bias

	gradientVector := inputs.Scale(conf.Functions.derivedActivation(weightedSum)).Scale(conf.Functions.derivedCost(expectedOut, out))

	return gradientVector
}

//MakeMultiNetwork returns a multi network ready for use
func MakeMultiNetwork(inputs int, dim []int, lrs album.NumAlbum, functs []Functions, layerTypes []MultiLayer) []Multi {
	if dim == nil {
		return nil
	}
	out := make([]Multi, len(dim))

	if functs == nil {
		functs = make([]Functions, len(dim))
	}
	configs := make([]LayerConf, len(dim))
	configs[0].Inputs = inputs
	configs[0].Lr = lrs[0]
	configs[0].Functs = functs[0]
	configs[0].Outputs = dim[0]
	fmt.Println(" |\n V\n", inputs, "\n", dim[0])

	for n := 1; n < len(configs); n++ {
		configs[n].Inputs = dim[n-1]
		configs[n].Outputs = dim[n]
		fmt.Println(" |\n V\n", dim[n-1], "\n", dim[n])
		configs[n].Lr = lrs[n]
		configs[n].Functs = functs[n]
	}

	for n := range out {
		out[n].Init(configs[n], layerTypes[n])
	}

	return out
}
