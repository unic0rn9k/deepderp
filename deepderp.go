package deepderp

import (
	"fmt"
	"math"
	"math/rand"
	"sync"

	"gitlab.com/unic0rn9k/album"
)

/*
  #
 #:#                  ##
 #: #    ###   ####  #: #
 #:  #  ##: #  #: #  #: #
 #:   # #:  #  #: #  #: #
 #:   # ####  #####  ###
 #:   # #:    #: #   #:
 #:  ## #:    #:  #  #:
 #:  #   ###  #:   # #:
 ####         #:     #:
                     #:

*/

//Functions struct contains necessary functions for the neural network to work
type Functions struct {
	activation        func(float64) float64
	derivedActivation func(float64) float64
	cost              func(float64, float64) float64
	derivedCost       func(float64, float64) float64
	isInUse           bool
}

//Config structure containing necessary configuration information for the neural network
type Config struct {
	NeuronsInLayers []int //0 is input layer and len()-1 is the output layer
	Inputs          int
	Lr              float64
	Functions       Functions
}

//VVVV MATH FUNCTIONS VVVV

//sech2 is the derivative of the tanh function
func sech2(x float64) float64 {
	return math.Pow((1 / math.Cosh(x)), 2)
}

//CostF : cost functions take the expected out as the first arg and the actual out as the second arg
func CostF(y, x float64) float64 {
	return math.Pow(y-x, 2)
}

func derivedCost(y, x float64) float64 {
	return (-2 * (y - x))
}

//Neuron ...pretty self explanetory
type Neuron struct {
	Weights    album.NumAlbum
	Bias       float64
	sync.Mutex //mutex for locking the neuron when running the code concurent
}

//^^^^ MATH FUNCTIONS ^^^^

// init function for the neurons. this function should be run before using a neuron
func (neo *Neuron) init(inputs int) {
	neo.Weights = make([]float64, inputs)
	for n := range neo.Weights {
		neo.Weights[n] = []float64{-1, 1}[rand.Intn(2)] * rand.Float64()
	}
	neo.Bias = []float64{-1, 1}[rand.Intn(2)] * rand.Float64()
}

//F returns predicted output for an input of a neuron
func (neo Neuron) F(inputs []float64, conf Config) float64 {
	return conf.Functions.activation(album.DotAlbum(neo.Weights, inputs) + neo.Bias)
}

//OutGradient returns gradient vector
func (neo Neuron) OutGradient(inputs []float64, expectedOut float64, conf Config) []float64 {
	out := neo.F(inputs, conf)
	gradientVector := make([]float64, len(neo.Weights))
	weightedSum := album.DotAlbum(neo.Weights, inputs) + neo.Bias

	for n := range neo.Weights {
		gradientVector[n] = inputs[n] * conf.Functions.derivedActivation(weightedSum) * conf.Functions.derivedCost(expectedOut, out)
	}

	return gradientVector
}

//OutInputSlope returns input slope, which is the sensitivity of the input in respect to the output
func (neo Neuron) OutInputSlope(inputs []float64, expectedOut float64, conf Config) []float64 {
	out := neo.F(inputs, conf)
	gradientVector := make([]float64, len(neo.Weights))
	weightedSum := album.DotAlbum(neo.Weights, inputs) + neo.Bias

	for n := range neo.Weights {
		gradientVector[n] = neo.Weights[n] * conf.Functions.derivedActivation(weightedSum) * conf.Functions.derivedCost(expectedOut, out)
	}

	return gradientVector
}

//OutBiasGradient returns bias gradient
func (neo Neuron) OutBiasGradient(inputs []float64, expectedOut float64, conf Config) float64 {
	out := neo.F(inputs, conf)
	weightedSum := album.DotAlbum(neo.Weights, inputs) + neo.Bias

	return conf.Functions.derivedActivation(weightedSum) * conf.Functions.derivedCost(expectedOut, out)
}

//HiddenGradient returns gradient vector and inputSlopes
func (neo Neuron) HiddenGradient(inputs []float64, nextLayersInputSlope float64, conf Config) ([]float64, []float64) {
	gradientVector := make([]float64, len(neo.Weights))
	inputSlopes := make([]float64, len(neo.Weights))
	weightedSum := album.DotAlbum(neo.Weights, inputs) + neo.Bias

	for n := range neo.Weights {
		gradientVector[n] = inputs[n] * conf.Functions.derivedActivation(weightedSum) * nextLayersInputSlope
		inputSlopes[n] = neo.Weights[n] * conf.Functions.derivedActivation(weightedSum) * nextLayersInputSlope
	}

	return gradientVector, inputSlopes
}

//HiddenBiasGradient returns gradient vector and inputSlopes
func (neo Neuron) HiddenBiasGradient(inputs []float64, nextLayersInputSlope float64, conf Config) float64 {
	weightedSum := album.DotAlbum(neo.Weights, inputs) + neo.Bias

	return conf.Functions.derivedActivation(weightedSum) * nextLayersInputSlope
}

//OutBackProbagate backpropagation function for output neurons
func (neo *Neuron) OutBackProbagate(inputs []float64, expectedOut float64, conf Config) []float64 { //returns cost value
	gradients := neo.OutGradient(inputs, expectedOut, conf)

	neo.Lock()
	for n := range neo.Weights {
		neo.Weights[n] += -1 * gradients[n] * conf.Lr
	}

	neo.Bias += -1 * neo.OutBiasGradient(inputs, expectedOut, conf) * conf.Lr
	neo.Unlock()

	return neo.OutInputSlope(inputs, expectedOut, conf)
}

//HiddenBackProbagate backpropagation function for hidden neurons
func (neo *Neuron) HiddenBackProbagate(inputs []float64, nextLayersInputSlope float64, conf Config) []float64 {
	gradients, inputSlopes := neo.HiddenGradient(inputs, nextLayersInputSlope, conf)

	neo.Lock()
	for n := range neo.Weights {
		neo.Weights[n] += -1 * gradients[n] * conf.Lr
	}

	neo.Bias += -1 * neo.HiddenBiasGradient(inputs, nextLayersInputSlope, conf) * conf.Lr
	neo.Unlock()

	return inputSlopes
}

//NeuralLayer just a slice of neurons
type NeuralLayer []Neuron

//DNN main deep neural network structure containing all the necessary information for a neural network
type DNN struct {
	DNN       []NeuralLayer
	DNNConfig Config
}

//FeedForward is the same as F but for a dnn instead of a neuron
func FeedForward(dnn DNN, inputs []float64) ([]float64, [][]float64) {
	layerOutputs := make([][]float64, len(dnn.DNN))
	for n := range layerOutputs {
		layerOutputs[n] = make([]float64, len(dnn.DNN[n]))
	}

	for n := range layerOutputs[0] {
		layerOutputs[0][n] = dnn.DNN[0][n].F(inputs, dnn.DNNConfig)
	}

	for l := 1; l < len(layerOutputs); l++ {
		for n := range layerOutputs[l] {
			layerOutputs[l][n] = dnn.DNN[l][n].F(layerOutputs[l-1], dnn.DNNConfig)
		}
	}

	return layerOutputs[len(layerOutputs)-1], layerOutputs
}

func sumOfArray(x []float64) float64 {
	var sum float64
	for _, n := range x {
		sum += n
	}
	return sum
}

//BackProbagateNet backpropagation function for a dnn
func BackProbagateNet(dnn *DNN, expectedOut, inputs []float64) error {
	if len((*dnn).DNN) < 2 {
		return fmt.Errorf("The network needs at least 2 layers for this function to work")
	}

	_, outMatrix := FeedForward(*dnn, inputs)
	outInputSlopes := make([]float64, len(((*dnn).DNN)[len((*dnn).DNN)-1][0].Weights))

	for n := range ((*dnn).DNN)[len((*dnn).DNN)-1] {
		slopesOfNeuron := (*dnn).DNN[len((*dnn).DNN)-1][n].OutBackProbagate(outMatrix[len(outMatrix)-2], expectedOut[n], (*dnn).DNNConfig)

		for w := range slopesOfNeuron {
			outInputSlopes[w] += slopesOfNeuron[w]
		}
	}

	var inputSlopes []float64

	for l := len(outMatrix) - 2; l >= 0; l-- {
		inputSlopes = make([]float64, len(((*dnn).DNN)[l][0].Weights))

		for n := range (*dnn).DNN[l] {
			var slopesOfNeuron []float64
			if l == 0 {
				slopesOfNeuron = (*dnn).DNN[l][n].HiddenBackProbagate(inputs, outInputSlopes[n], dnn.DNNConfig)
			} else {
				slopesOfNeuron = (*dnn).DNN[l][n].HiddenBackProbagate(outMatrix[l-1], outInputSlopes[n], dnn.DNNConfig)
				for w := range slopesOfNeuron {
					inputSlopes[w] += slopesOfNeuron[w]
				}
			}

		}
		outInputSlopes = inputSlopes
	}

	return nil
}

//InitDNN initializes a dnn. WARNING: this function should allways be called before using a neural network
func InitDNN(dnn *DNN, config Config) {
	(*dnn).DNNConfig = config
	(*dnn).DNN = make([]NeuralLayer, len(config.NeuronsInLayers))
	if !config.Functions.isInUse {
		dnn.DNNConfig.Functions = Functions{math.Tanh, sech2, CostF, derivedCost, true}
	}

	for l := 0; l < len(config.NeuronsInLayers); l++ {
		(*dnn).DNN[l] = make([]Neuron, config.NeuronsInLayers[l])
		for n := 0; n < config.NeuronsInLayers[l]; n++ {

			if l == 0 {
				(*dnn).DNN[l][n].init(config.Inputs)
			} else {
				(*dnn).DNN[l][n].init(config.NeuronsInLayers[l-1])
			}
		}
	}
}
